﻿using batalha_naval.Models;
using System.Collections.Generic;
using System.Linq;

namespace batalha_naval.Lib
{
    public static class Game
    {
        public static ICollection<Celula> ConverteMatrizParaCelulas(ICollection<ICollection<Celula>> matriz)
        {
            var celulas = new List<Celula>();
            for (int i = 0; i < matriz.Count; i++)
            {
                for (int j = 0; j < matriz.ElementAt(i).Count; j++)
                {
                    var celula = matriz.ElementAt(i).ElementAt(j);
                    celula.PosicaoI = i;
                    celula.PosicaoJ = j;
                    celulas.Add(celula);
                }
            }
            return celulas;
        }

        public static Celula[][] ConverteCelulasParaMatriz(ICollection<Celula> celulas)
        {
            var matriz = new Celula[celulas.Max(x => x.PosicaoI)][];
            foreach (var celula in celulas)
            {
                if (matriz[celula.PosicaoI] == null)
                {
                    matriz[celula.PosicaoI] = new Celula[celulas.Max(x => x.PosicaoJ)];
                }

                matriz[celula.PosicaoI][celula.PosicaoJ] = celula;
            }
            return matriz;
        }
    }
}
