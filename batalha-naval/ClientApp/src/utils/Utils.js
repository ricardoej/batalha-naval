﻿function inserirNavio(campo, tamanho) {
    let posicao = getPosicaoAleatoria(campo.length, campo[0].length);
    let inseriu = false;
    while (!inseriu) {
        inseriu = tentaInserir(posicao, tamanho, campo);
        if (!inseriu) {
            posicao = getProximaPosicao(posicao, campo.length, campo[0].length);
        }
    }
}

function getPosicaoAleatoria(alturaMaxima, larguraMaxima) {
    return {
        altura: getIntAleatorio(0, alturaMaxima - 1),
        largura: getIntAleatorio(0, larguraMaxima - 1)
    };
}

function getIntAleatorio(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function tentaInserir(posicao, tamanho, campo) {
    const orientacao = getOrientacao();
    if (inserir(posicao, orientacao, tamanho, campo)) {
        return true;
    } else {
        let proximaOrientacao = getProximaOrientacao(orientacao);
        while (JSON.stringify(proximaOrientacao) !== JSON.stringify(orientacao)) {
            if (inserir(posicao, proximaOrientacao, tamanho, campo)) {
                return true;
            } else {
                proximaOrientacao = getProximaOrientacao(proximaOrientacao);
            }
        }
        return false;
    }
}

function getOrientacao() {
    const stepAltura = getIntAleatorio(-1, 1);
    let randLargura = getIntAleatorio(-1, 1);
    if (randLargura === 0) randLargura = 1;
    const stepLargura = stepAltura !== 0 ? 0 : randLargura;
    return {
        stepAltura: stepAltura,
        stepLargura: stepLargura
    };
}

function inserir(posicao, orientacao, tamanho, campo) {
    if (existeEspaco(posicao, orientacao, tamanho, campo) && !espacoOcupado(posicao, orientacao, tamanho, campo)) {
        for (var i = 0; i < tamanho; i++) {
            const altura = posicao.altura + (orientacao.stepAltura * i);
            const largura = posicao.largura + (orientacao.stepLargura * i);
            campo[altura][largura] = {
                isNavio: true,
                isEscondido: false,
                isAdjacente: false
            };
        }

        inserirAdjacentes(campo);

        return true;
    }

    return false;
}

function existeEspaco(posicao, orientacao, tamanho, campo) {
    const alturaFinal = posicao.altura + (orientacao.stepAltura * (tamanho - 1));
    const larguraFinal = posicao.largura + (orientacao.stepLargura * (tamanho - 1));
    return alturaFinal < campo.length
        && alturaFinal >= 0
        && larguraFinal < campo[0].length
        && larguraFinal >= 0;
}

function espacoOcupado(posicao, orientacao, tamanho, campo) {
    for (var i = 0; i < tamanho; i++) {
        const altura = posicao.altura + (orientacao.stepAltura * i);
        const largura = posicao.largura + (orientacao.stepLargura * i);
        if (campo[altura][largura]) {
            return true;
        }
    }
    return false;
}

function getProximaOrientacao(orientacao) {
    return {
        stepAltura: orientacao.stepAltura !== 0 ? 0 : orientacao.stepLargura * -1,
        stepLargura: orientacao.stepLargura !== 0 ? 0 : orientacao.stepAltura,
    };
}

function getProximaPosicao(posicao, alturaMaxima, larguraMaxima) {
    let proximaLargura = posicao.largura + 1;
    let proximaAltura = posicao.altura;
    if (proximaLargura >= larguraMaxima) {
        proximaLargura = 0;
        proximaAltura = posicao.altura + 1;
        if (proximaAltura >= alturaMaxima) {
            proximaAltura = 0;
        }
    }

    return {
        altura: proximaAltura,
        largura: proximaLargura
    };
}

function inserirAdjacentes(campo) {
    for (var i = 0; i < campo.length; i++) {
        for (var j = 0; j < campo[i].length; j++) {
            if (!campo[i][j] && isAdjacente(campo, i, j)) {
                campo[i][j] = {
                    isNavio: false,
                    isEscondido: false,
                    isAdjacente: true
                };
            }
        }
    }
}

function isAdjacente(campo, altura, largura) {
    if (campo[altura - 1] && campo[altura - 1][largura] && !campo[altura - 1][largura].isAdjacente) return true;
    if (campo[altura + 1] && campo[altura + 1][largura] && !campo[altura + 1][largura].isAdjacente) return true;
    if (campo[altura] && campo[altura][largura - 1] && !campo[altura][largura - 1].isAdjacente) return true;
    if (campo[altura] && campo[altura][largura + 1] && !campo[altura][largura + 1].isAdjacente) return true;
    if (campo[altura - 1] && campo[altura - 1][largura + 1] && !campo[altura - 1][largura + 1].isAdjacente) return true;
    if (campo[altura - 1] && campo[altura - 1][largura - 1] && !campo[altura - 1][largura - 1].isAdjacente) return true;
    if (campo[altura + 1] && campo[altura + 1][largura - 1] && !campo[altura + 1][largura - 1].isAdjacente) return true;
    if (campo[altura + 1] && campo[altura + 1][largura + 1] && !campo[altura + 1][largura + 1].isAdjacente) return true;
    return false;
}

function inserirMar(campo) {
    for (var i = 0; i < campo.length; i++) {
        for (var j = 0; j < campo[i].length; j++) {
            if (!campo[i][j]) {
                campo[i][j] = {
                    isNavio: false,
                    isEscondido: false,
                    isAdjacente: false
                };
            }
        }
    }
}

function esconderTodasCelulas(campo) {
    for (var i = 0; i < campo.length; i++) {
        for (var j = 0; j < campo[i].length; j++) {
            if (campo[i][j]) {
                campo[i][j].isEscondido = true;
            }
        }
    }
}

const utils = {
    gerarCampoAleatorio(altura, largura, isEscondido = false) {
        if (altura === 0 || largura === 0) return null;

        let campo = [];
        for (var i = 0; i < altura; i++) {
            campo[i] = [];
            for (var j = 0; j < largura; j++) {
                campo[i][j] = undefined;
            }
        }

        inserirNavio(campo, 4);
        inserirNavio(campo, 3);
        inserirNavio(campo, 3);
        inserirNavio(campo, 2);
        inserirNavio(campo, 2);
        inserirNavio(campo, 2);
        inserirNavio(campo, 1);
        inserirNavio(campo, 1);
        inserirNavio(campo, 1);
        inserirNavio(campo, 1);
        inserirMar(campo);

        if (isEscondido) {
            esconderTodasCelulas(campo);
        }

        return campo;
    }
}

export const Utils = utils