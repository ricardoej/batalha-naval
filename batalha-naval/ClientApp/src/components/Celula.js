﻿import React, { Component } from 'react';
import './Celula.css';

export class Celula extends Component {
    constructor(props) {
        super(props);

        this.tratarCelularClick = this.tratarCelularClick.bind(this);
    }

    tratarCelularClick() {
        this.props.onCelulaClick(this.props.altura, this.props.largura);
    }

    render() {
        let className = 'celula ';
        if (this.props.formato.isEscondido) {
            className += 'isEscondido ';
        }
        else {
            if (this.props.formato.isMarcado) {
                className += 'isMarcado ';
            }

            if (this.props.formato.isNavio) {
                className += 'isNavio';
            } else {
                className += 'isOceano';
            }
        }

        return (<div className={className} onClick={this.tratarCelularClick}/>);
    }
}