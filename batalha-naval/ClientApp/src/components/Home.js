﻿import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';

export class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div>
                <h1>Bem vindo ao jogo Batalha Naval!</h1>
                <p>Batalha naval é um jogo de tabuleiro de dois jogadores, no qual os jogadores têm de adivinhar em que quadrados estão os navios do oponente.</p>
                <p>O jogo é jogado em dois campos de dimensões 10X10 para cada jogador — uma que representa a disposição dos barcos do jogador, e outra que representa a do oponente. Os campos são tipicamente quadrados, estando identificadas na horizontal por números e na vertical por letras. Em cada campo o jogador coloca os seus navios e regista os tiros do oponente.</p>
                <ul>
                    <li>Antes do início do jogo, cada jogador coloca os seus navios nos quadros, alinhados horizontalmente ou verticalmente. O número de navios permitidos é igual para ambos jogadores e os navios não podem se sobrepor.</li>
                    <li>Após os navios terem sido posicionados, o jogo continua numa série de turnos. Em cada turno, um jogador escolhe um quadrado no campo do oponente. Se houver um navio nesse quadrado, é colocada uma marca vermelha, senão houver é colocada uma marca branca.</li>
                    <li>Os tipos de navios são: porta-aviões (quatro quadrados), navios-tanque (três quadrados), contratorpedeiros (dois quadrados) e submarinos (um quadrado). Vale notar que os quadrados que compõem um navio devem estar conectados e em fila reta.</li>
                </ul>
                <Link to="/iniciar">
                    <Button color="primary">Iniciar jogo!</Button>
                </Link>
            </div>
        );
    }
}
