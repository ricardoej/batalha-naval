﻿import React, { Component } from 'react';
import { Row, Col, Button, Label, Input, InputGroup, InputGroupAddon, Modal, ModalHeader, ModalBody, ModalFooter, FormFeedback, FormGroup } from 'reactstrap';
import { Board } from './Board';
import { Link } from 'react-router-dom';
import { Celula } from './Celula';
import { Utils } from '../utils/Utils'
import './IniciarJogo.css';

export class IniciarJogo extends Component {
    constructor(props) {
        super(props);

        this.alturaBoard = 10;
        this.larguraBoard = 10;

        this.state = {
            campo: Utils.gerarCampoAleatorio(this.larguraBoard, this.alturaBoard),
            jogoSelecionado: null,
            nomeJogador: null,
            modalIsVisible: false,
            jogos: [
                {
                    id: 1,
                    nome: 'Jogo 1'
                },
                {
                    id: 2,
                    nome: 'Jogo 2'
                },
                {
                    id: 3,
                    nome : 'Jogo 3'
                }
            ]
        }

        this.exibirEsconderModal = this.exibirEsconderModal.bind(this);
        this.gerarCampo = this.gerarCampo.bind(this);
        this.handleJogoSelecionadoChange = this.handleJogoSelecionadoChange.bind(this);
        this.handleNomeJogadorChange = this.handleNomeJogadorChange.bind(this);
    }

    handleJogoSelecionadoChange(e) {
        this.setState({
            jogoSelecionado: e.target.value
        });
    }

    handleNomeJogadorChange(e) {
        this.setState({
            nomeJogador: e.target.value
        });
    }

    gerarCampo() {
        this.setState({
            campo: Utils.gerarCampoAleatorio(this.larguraBoard, this.alturaBoard)
        });
    }

    exibirEsconderModal() {
        this.setState(prevState => ({
            modalIsVisible: !prevState.modalIsVisible
        }));
    }

    renderNavio(nome, tamanhoNavio, quantidade) {
        const navio = <Celula formato={{ isNavio: true }} />;
        const texto = nome + ' (' + quantidade + 'x)';

        // A função Array(x).fill(elemento) criar um array de tamanho x
        // e preenche ele com o valor elemento
        return (
            <div>
                <Row>
                    <Col>{texto}</Col>
                </Row>
                <Row>
                    <Col>{Array(tamanhoNavio).fill(navio)}</Col>
                </Row>
            </div>
        );
    }

    renderComandos() {
        return (
            <div>
                <div>
                    {this.renderNavio('Porta-avião', 4, 1)}
                    {this.renderNavio('Navio-tanque', 3, 2)}
                    {this.renderNavio('Contratorpedeiro', 2, 3)}
                    {this.renderNavio('Submarino', 1, 4)}
                </div>
                <div className="game-escolha">
                    <FormGroup>
                        <Label>Jogo</Label>
                        <InputGroup>
                            <Input type="select" value={this.state.jogoSelecionado} onChange={this.handleJogoSelecionadoChange}>
                                { this.renderSelectOptions() }
                            </Input>
                            <InputGroupAddon addonType="append"><Button color="link" onClick={this.exibirEsconderModal}>Novo Jogo</Button></InputGroupAddon>
                        </InputGroup>
                    </FormGroup>
                    <FormGroup>
                        <Label>Jogador</Label>
                        <Input type="text" value={this.state.nomeJogador} onChange={this.handleNomeJogadorChange} />
                    </FormGroup>
                </div>
                {this.renderModal()}
                <div>
                    <Button className="comandos" color="secondary" onClick={this.gerarCampo}>Gerar campo</Button>
                    <Button className="comandos" color="primary" disabled={!this.state.nomeJogador || !this.state.jogoSelecionado} onClick={this.jogar}>Jogar</Button>
                </div>
            </div>
        );
    }

    renderSelectOptions() {
        return this.state.jogos.map((jogo) =>
            <option key={jogo.id}>{jogo.nome}</option>
        );
    }

    renderModal() {
        return (
            <div>
                <Modal isOpen={this.state.modalIsVisible} toggle={this.exibirEsconderModal}>
                    <ModalHeader toggle={this.exibirEsconderModal}>Inserir novo jogo</ModalHeader>
                    <ModalBody>
                        
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary">Inserir</Button>{' '}
                        <Button color="secondary" onClick={this.exibirEsconderModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

    render() {
        return (
            <div>
                <Row>
                    <Col>
                        <div className="game-info">
                            <h3>Monte o seu campo de batalha e escolha a sua estratégia!</h3>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Board celulas={this.state.campo}/>
                    </Col>
                    <Col>
                        {this.renderComandos()}
                    </Col>
                </Row>
            </div>
        );
    }
}