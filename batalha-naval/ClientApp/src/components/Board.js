﻿import React, { Component } from 'react';
import { Celula } from './Celula';
import './Board.css';

export class Board extends Component {
    renderBoard() {
        if (!this.props.celulas) {
            return <div>Não existem células para serem exibidas</div>
        }

        let linhas = [];
        for (var i = 0; i < this.props.celulas.length; i++) {
            let colunas = [];
            for (var j = 0; j < this.props.celulas[i].length; j++) {
                colunas.push(<Celula
                    key={'(' + i + ',' + j + ')'}
                    formato={this.props.celulas[i][j]}
                    onCelulaClick={this.props.onCelulaClick}
                    altura={i}
                    largura={j}
                />)
            }
            linhas.push(<div className="celula-linha">{colunas}</div>);
        }
        return linhas;
    }

    render() {
        return (
            <div>
                {this.renderBoard()}
            </div>
        );
    }
}