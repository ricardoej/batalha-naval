﻿import React, { Component } from 'react';
import { Col, Row, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { FaArrowUp } from 'react-icons/fa';
import { Board } from './Board';

export class Jogo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            campoJogador: null,
            campoOponente: null,
            turnoOponente: false
        }
    }

    renderExisteJogo() {
        return (
            <div>
                <Row>
                    <Col>
                        <div className="espaco-board">
                            <Board celulas={this.state.campoJogador} />
                            {!this.state.turnoOponente && <FaArrowUp size={64} />}
                        </div>
                    </Col>
                    <Col>
                        <div className="espaco-board">
                            <Board celulas={this.state.campoOponente} onCelulaClick={this.tratarCelulaClick} />
                            {this.state.turnoOponente && <FaArrowUp size={64} />}
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    renderNaoExisteJogo() {
        return (
            <div className="d-flex justify-content-center">
                <Link to="/iniciar">
                    <Button size="lg" block color="primary">Monte o seu campo de batalha antes de jogar!</Button>
                </Link>
            </div>
        );
    }

    render() {
        if (this.props.location.jogo && this.state.campoJogador && this.state.campoOponente) {
            return this.renderExisteJogo();
        } else {
            return this.renderNaoExisteJogo();
        }
    }
}