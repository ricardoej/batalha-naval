import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { IniciarJogo } from './components/IniciarJogo';
import { Jogo } from './components/Jogo';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
            <Route exact path='/' component={Home} />
            <Route path='/iniciar' component={IniciarJogo} />
            <Route path='/jogo' component={Jogo} />
      </Layout>
    );
  }
}
