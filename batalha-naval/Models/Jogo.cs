﻿namespace batalha_naval.Models
{
    public class Jogo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int? Jogador1Id { get; set; }
        public virtual Jogador Jogador1 { get; set; }
        public int? Jogador2Id { get; set; }
        public virtual Jogador Jogador2 { get; set; }

    }
}
