﻿using System.Collections.Generic;

namespace batalha_naval.Models
{
    public class Board
    {
        public int Id { get; set; }
        public int JogadorId { get; set; }
        public virtual ICollection<Celula> Celulas { get; set; }
    }
}