﻿namespace batalha_naval.Models
{
    public class Celula
    {
        public int Id { get; set; }
        public int BoardId { get; set; }
        public bool IsNavio { get; set; }
        public bool IsOceano { get; set; }
        public bool IsMarcado { get; set; }
        public int PosicaoI { get; set; }
        public int PosicaoJ { get; set; }
    }
}