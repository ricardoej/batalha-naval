﻿namespace batalha_naval.Models
{
    public class Jogador
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public virtual Board Board { get; set; }
        public bool IsVencedor { get; set; }
    }
}